package com.example.unittests

import org.junit.Test

import org.junit.Assert.*

class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun plus() {
        assertEquals("4", (2 + 2).toString())
    }
}