package com.example.unittests

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.text.isDigitsOnly

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.button1)
        val edText = findViewById<EditText>(R.id.editTextText)
        val tvAnswer = findViewById<TextView>(R.id.textView)

        button.setOnClickListener {
            plus(edText.text.toString())
        }
    }

    fun plus(str: String) : String {
        var stroka = str.filterNot { str.isDigitsOnly() }
        return stroka
    }

}